import { Grid } from '@mui/material';
import { World, Controlls, Statistic, Configurator } from './components';

const App = () => {
  return (
    <Grid container>
      <Grid item>
        <World />
      </Grid>
      <Grid item flexGrow={1}>
        <Configurator />
        <Statistic />
      </Grid>
      <Grid item xs={12}>
        <Controlls />
      </Grid>
    </Grid>
  );
};

export default App;
