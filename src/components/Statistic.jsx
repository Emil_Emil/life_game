import React from 'react';
import { inject, observer } from 'mobx-react';
import { Grid, TextField, Paper } from '@mui/material';
import { compose } from '../utils';

export const Statistic = compose(
  inject('store'),
  observer
)(({ store: { generation } }) => {
  return (
    <Paper>
      <Grid container>
        <Grid item>
          <TextField
            name='generation'
            size='small'
            label='Поколение'
            readOnly
            value={generation}
          ></TextField>
        </Grid>
      </Grid>
    </Paper>
  );
});
