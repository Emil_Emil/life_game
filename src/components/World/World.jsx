import { WorldRow } from './WorldRow';
import { inject, observer } from 'mobx-react';
import { compose } from '../../utils';

export const World = compose(
  inject('store'),
  observer
)(({ store: { world } }) => {
  return (
    <table style={{ borderCollapse: 'collapse' }}>
      <tbody>
        {world.map((row, index) => (
          <WorldRow key={index} row={row} />
        ))}
      </tbody>
    </table>
  );
});
