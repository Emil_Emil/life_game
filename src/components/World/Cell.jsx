import React, { memo } from 'react';

export const Cell = memo(({ alive = false }) => {
  return <td className={'cell' + (alive ? ' alive' : '')}></td>;
});
