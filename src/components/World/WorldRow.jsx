import React, { memo } from 'react';
import { Cell } from './Cell';

export const WorldRow = memo(
  ({ row }) => (
    <tr>
      {row.map((alive, index) => (
        <Cell key={index} alive={alive} />
      ))}
    </tr>
  ),
  function arePropsEqual(prevProps, nextProps) {
    return prevProps.row.join('') === nextProps.row.join('');
  }
);
