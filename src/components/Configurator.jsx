import React from 'react';
import { inject, observer } from 'mobx-react';
import { TextField, Grid, Paper } from '@mui/material';
import { compose } from '../utils';

export const Configurator = compose(
  inject('store'),
  observer
)(({ store: { width, height, probability, change } }) => {
  function onChange({ target: { name, value } }) {
    change(name, +value);
  }
  return (
    <Paper>
      <Grid container>
        <Grid item xs={12}>
          <TextField
            type='number'
            label='Высота'
            name='height'
            value={height}
            onChange={onChange}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            type='number'
            label='Ширина'
            name='width'
            value={width}
            onChange={onChange}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            type='number'
            label='Вероятность жизни'
            name='probability'
            value={probability}
            onChange={onChange}
            helperText='Для случайной генерации'
          />
        </Grid>
      </Grid>
    </Paper>
  );
});
