import React from 'react';
import { inject, observer } from 'mobx-react';
import { Button } from '@mui/material';
import { compose } from '../utils';

export const Controlls = compose(
  inject('store'),
  observer
)(
  ({
    store: {
      gameOver = false,
      clearWorld = () => {},
      generateNext = () => {},
      generateRandomWorld = () => {},
      toggleLife = () => {},
      lifeStarted = false,
      change,
    },
  }) => {
    return (
      <div>
        <Button variant='filled' onClick={toggleLife}>
          {lifeStarted ? 'Stop' : 'Start'}
        </Button>
        <Button variant='filled' onClick={generateNext} disabled={gameOver}>
          Next
        </Button>
        <Button variant='filled' onClick={clearWorld}>
          Reset
        </Button>
        <Button
          variant='filled'
          onClick={() => {
            change('probability', Math.random() / 3);
            generateRandomWorld();
          }}
        >
          New Random World
        </Button>
      </div>
    );
  }
);
