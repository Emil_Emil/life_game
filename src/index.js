import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import { createTheme, ThemeProvider } from '@mui/material/styles';

import './index.css';
import App from './App';
import { Store } from './Store';

const theme = createTheme({});

const store = Store.create({});
window.store = store;
ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <App />
    </Provider>
  </ThemeProvider>,
  document.getElementById('root')
);
