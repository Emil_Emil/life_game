const workercode = () => {
  // eslint-disable-next-line no-restricted-globals
  self.onmessage = function ({ data: { world, generation } }) {
    let wasChanged = false;
    function getCellState(currentState, neightboursCount) {
      const nextState =
        neightboursCount === 3 || (currentState && neightboursCount === 2);
      wasChanged = wasChanged || nextState !== currentState;
      return nextState;
    }

    function getCellAliveNeighboursCount(rowIndex, columnIndex) {
      const neightbours = [
        [rowIndex - 1, columnIndex],
        [rowIndex - 1, columnIndex + 1],
        [rowIndex, columnIndex - 1],
        [rowIndex, columnIndex + 1],
        [rowIndex + 1, columnIndex - 1],
        [rowIndex + 1, columnIndex],
        [rowIndex + 1, columnIndex + 1],
        [rowIndex - 1, columnIndex - 1],
      ];

      return neightbours.reduce(
        (acc, [i, j]) => acc + (world?.[+i]?.[+j] || 0),
        0
      );
    }

    postMessage({
      world: world.map((worldRow, rowIndex) =>
        worldRow.map((cellState, columnIndex) =>
          getCellState(
            cellState,
            getCellAliveNeighboursCount(rowIndex, columnIndex)
          )
        )
      ),
      wasChanged,
      generation,
    });
  };
};
let code = workercode.toString();
code = code.substring(code.indexOf('{') + 1, code.lastIndexOf('}'));

const blob = new Blob([code], { type: 'application/javascript' });
export const workerScript = URL.createObjectURL(blob);
