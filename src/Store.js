import { types } from 'mobx-state-tree';

export const Store = types
  .model('Store', {
    generation: 1,
    width: 50,
    height: 50,
    interval: 500,
    probability: 0.05,
    world: types.frozen([]),
    gameOver: false,
    lifeStarted: false,
  })
  .actions((self) => {
    const afterCreate = () => {
      self.generateRandomWorld();
    };
    const change = (name, value) => {
      self[name] = value;
      if (name === 'width' || name === 'height') {
        generateRandomWorld();
      }
    };
    const generateRandomWorld = () => {
      self.resetWorld();
      self.world = Array(self.height)
        .fill()
        .map(() =>
          Array(self.width)
            .fill()
            .map(() => Math.random() < self.probability)
        );
    };
    const resetWorld = () => {
      self.gameOver = false;
      self.lifeStarted = false;
      self.generation = 1;
    };

    const clearWorld = () => {
      self.resetWorld();
      self.probability = 0;
      self.generateRandomWorld();
    };
    const setWorld = (world, wasChanged) => {
      self.world = world;
      self.generation += 1;
      if (!wasChanged) {
        self.gameOver = true;
        self.lifeStarted = false;
      }
    };
    const generateNext = () => {
      setTimeout(() => {
        let wasChanged = false;
        const currentWorld = self.world;
        function getCellState(row, col) {
          const currentState = currentWorld[row][col];
          const neightboursCount = getCellAliveNeighboursCount(row, col);
          const nextState =
            neightboursCount === 3 || (currentState && neightboursCount === 2);
          wasChanged = wasChanged || nextState !== currentState;
          return nextState;
        }

        function getCellAliveNeighboursCount(rowIndex, columnIndex) {
          const neightbours = [
            [rowIndex - 1, columnIndex],
            [rowIndex - 1, columnIndex + 1],
            [rowIndex, columnIndex - 1],
            [rowIndex, columnIndex + 1],
            [rowIndex + 1, columnIndex - 1],
            [rowIndex + 1, columnIndex],
            [rowIndex + 1, columnIndex + 1],
            [rowIndex - 1, columnIndex - 1],
          ];

          return neightbours.reduce(
            (acc, [i, j]) => acc + (currentWorld?.[+i]?.[+j] || 0),
            0
          );
        }

        const world = [];

        for (let rowIndex = 0; rowIndex < self.height; rowIndex++) {
          const row = [];
          for (let colIndex = 0; colIndex < self.width; colIndex++) {
            row.push(getCellState(rowIndex, colIndex));
          }
          world.push(row);
        }
        self.setWorld(world, wasChanged);
        if (self.lifeStarted && wasChanged) setTimeout(autoGenerateNext, 0);
      }, 0);
    };
    const autoGenerateNext = () => {
      if (self.lifeStarted) generateNext();
    };
    const toggleLife = () => {
      self.lifeStarted ? stopLife() : startLife();
    };
    const stopLife = () => {
      self.lifeStarted = false;
    };
    const startLife = () => {
      if (self.gameOver) return;
      self.lifeStarted = true;
      generateNext();
    };
    return {
      afterCreate,
      change,
      generateNext,
      setWorld,
      resetWorld,
      generateRandomWorld,
      autoGenerateNext,
      clearWorld,
      toggleLife,
    };
  });
